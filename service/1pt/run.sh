#!/data/data/com.termux/files/usr/bin/bash
short=$(curl -s "https://api.1pt.co/addURL?long=$1" | jq --raw-output '.short')
url="https://1pt.co/$short"
termux-notification \
  --icon link \
  -t "URL Shortened" -c "$url" \
  --button1 "COPY" \
  --button1-action "termux-clipboard-set $url"

#!/data/data/com.termux/files/usr/bin/bash
#https://exchangerate.host
. ./config
amount=$(echo "$1" | sed 's/[^0-9.]*//g')
echo "https://api.exchangerate.host/latest?base=$base&amount=$amount"
res=$(curl -s "https://api.exchangerate.host/latest?base=$base&amount=$amount")
echo $res
#TODO: detect current currency and convert it to home currency

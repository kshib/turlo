#!/data/data/com.termux/files/usr/bin/bash
. $SCRIPT_DIR/service/fileio/config
url=$(curl -sF "file=@$1" https://file.io | jq --raw-output '.link')
termux-notification \
  --icon file_upload \
  -t "File uploaded" -c "$url" \
  --button1 "COPY" \
  --button1-action "termux-clipboard-set $url"

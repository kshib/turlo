#!/data/data/com.termux/files/usr/bin/bash
outline_res=$(curl -s "https://api.outline.com/v3/parse_article?source_url=$1")
code=$(printf "%s" $outline_res | jq --raw-output '.data.short_code')
title=$(printf "%s" $outline_res | jq --raw-output '.data.title')
description=$(printf "%s" $outline_res | jq --raw-output '.data.meta.description')
url="https://outline.com/$code"
termux-notification \
  --icon article \
  -t "$title" -c "$description" \
  --button1 "COPY" \
  --button1-action "termux-clipboard-set $url" \
  --button2 "OPEN" \
  --button2-action "xdg-open $url"

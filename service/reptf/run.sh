#!/data/data/com.termux/files/usr/bin/bash
if [[ "$1" != "https://steamcommunity.com/profiles/"* && "$1" != "https://steamcommunity.com/id/"* ]]; then
    echo "Invalid url $1"
    exit 1
fi
api="https://rep.tf/api/transform?str=$1"
steamid=$(curl -s $api | jq --raw-output '.steamid')
echo $(curl -s -X POST -F "str=$steamid" "https://rep.tf/api/bans")

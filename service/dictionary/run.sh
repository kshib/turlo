#!/data/data/com.termux/files/usr/bin/bash
#https://dictionaryapi.dev/
. $SCRIPT_DIR/service/dictionary/config

word=$(cat "$1")
res=$(curl -s "https://api.dictionaryapi.dev/api/v2/entries/$lang/$word")

# check for error / not found
title=$(echo $res | jq --raw-output '.title')
if [[ -z $title ]]; then
  message=$(echo $res | jq --raw-output '.title')
  resolution=$(echo $res | jq --raw-output '.resolution')
  termux-notification \
    --icon error \
    -t "$message" \
    -c "$resolution"
  exit 1
fi

# notify result
definition=$(echo $res | jq --raw-output '.[0].meanings[0].definitions[0].definition')
example=$(echo $res | jq --raw-output '.[0].meanings[0].definitions[0].example')
word=$(echo $res | jq --raw-output '.[0].word')
speech=$(echo $res | jq --raw-output '.[0].meanings[0].partOfSpeech')
termux-notification \
  --icon book \
  -t "$word [$speech]" \
  -c "$definition
Usage: $example"

#!/data/data/com.termux/files/usr/bin/bash
. $SCRIPT_DIR/service/microlink/config

url_to_pdf () {
  response=$(curl -s "https://api.microlink.io?url=$url&pdf&scale=$scale&margin=$margin&adblock=true&waitForTimeout=$timeout")
  pdf_url=$(echo $response | jq --raw-output '.data.pdf.url')
  title=$(echo $response | jq --raw-output '.data.title' | tr -dc '[:alnum:] \n\r' )
  curl -s -o "DOCUMENTS_PATH/$title.pdf" $pdf_url
  termux-notification \
    --icon article \
    -t "$title" -c "$description" \
    --button1 "COPY" \
    --button1-action "termux-clipboard-set $url" \
    --button2 "OPEN" \
    --button2-action "xdg-open $url"
}
